package 
{
	import flash.display.Shape;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.KeyboardEvent;
	import flash.events.MouseEvent;
	import flash.events.TimerEvent;
	import flash.geom.ColorTransform;
	import flash.geom.Transform;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import flash.ui.Keyboard;
	import flash.system.fscommand;
	import flash.utils.Timer;
	
	/**
	 * ...
	 * @author Vimarsh Raina
	 */
	public class Main extends Sprite 
	{		
		[Embed(source = "../soundtrack.mp3")]
		private var soundTrackClass:Class;
		private var soundTrack:Sound = new soundTrackClass;
		private var soundTrackChannel:SoundChannel = new SoundChannel();
		
		private var map:XML;
		private var myLoader:URLLoader = new URLLoader();
		private var mapLoaded:Boolean = false;
		
		private var game:Dimensions = new Dimensions();
		
		private var background:Shape = new Shape();
		
		private var gameState:int = 0;
		
		private var menu:MainMenu = new MainMenu();
		
		private var dimensionIndicator:TextField = new TextField();
		private var levelComplete:TextField = new TextField();
		private var timerIndicator:TextField = new TextField();
		
		private var timer:Timer = new Timer(1000, 10);

		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			
			addEventListener(Event.ENTER_FRAME, onEnterFrame);
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
			stage.addEventListener(MouseEvent.CLICK, onClick);
			
			timer.addEventListener(TimerEvent.TIMER_COMPLETE, onTimerEnd);
			timer.toString();
			
			var tF:TextFormat = new TextFormat();
			tF.align = "left";
			tF.size = 30;
			
			dimensionIndicator.textColor = 0xFFFFFF;
			dimensionIndicator.width = 500;
			dimensionIndicator.height = 150;
			dimensionIndicator.defaultTextFormat = tF;
			dimensionIndicator.text = "Dimension: " + String(game.GetCurrentDimension);
			dimensionIndicator.x = 0;
			dimensionIndicator.y = 0;
			
			timerIndicator.textColor = 0xFFFFFF;
			timerIndicator.width = 100;
			timerIndicator.height = 50;
			timerIndicator.defaultTextFormat = tF;
			timerIndicator.text = String(timer.currentCount);
			timerIndicator.x = 990;
			timerIndicator.y = 0;
			
			tF = new TextFormat();
			tF.align = "center";
			tF.size = 60;
			
			levelComplete.textColor = 0xFFFFFF;
			levelComplete.border = true;
			levelComplete.borderColor = 0xFF0000;
			levelComplete.width = 500;
			levelComplete.height = 230;
			levelComplete.defaultTextFormat = tF;
			levelComplete.multiline = true;
			levelComplete.wordWrap = true;
			levelComplete.text = "Level Complete\n Press Escape to go\n back to Menu";
			levelComplete.x = 300;
			levelComplete.y = 200;
			
			stage.addChild(menu);
			menu.Init();
			
			soundTrackChannel = soundTrack.play(0, 0);
			soundTrackChannel.addEventListener(Event.SOUND_COMPLETE, LoopMusic);
			
		}
		
		private function  LoopMusic(e:Event):void
		{
			soundTrackChannel.removeEventListener(Event.SOUND_COMPLETE, LoopMusic);
			soundTrackChannel = soundTrack.play();
			soundTrackChannel.addEventListener(Event.SOUND_COMPLETE, LoopMusic);
		}
		
		private function LoadMap(name:String):void
		{
			myLoader.load(new URLRequest (name));
			myLoader.addEventListener(Event.COMPLETE, processXML);
			myLoader.addEventListener(IOErrorEvent.IO_ERROR, MapLoaderFail);
		}
		
		private function processXML(e:Event = null):void
		{
			/*************************/
			stage.removeChild(menu);
			
			game = null;
			game = new Dimensions();
			
			background.graphics.beginFill(0xFFFFFF, 1);
			background.graphics.drawRect(0, 0, stage.stageWidth, stage.stageHeight);
			background.graphics.endFill();
			
			stage.addChild(background);
			
			stage.addChild(game);
			
			timer.start();
			/********************/
			
			map = new XML(e.target.data);
			
			for (var i:int = 0; i < map.Cogs.length(); ++i)
			{
				var tempCog:Cog = new Cog();
				tempCog.Init(map.Cogs[i].x, map.Cogs[i].y, map.Cogs[i].radius, 1);
				
				stage.addChild(tempCog);
				game.AddCog(tempCog);
			}
			
			for (i = 0; i < map.Dimension.length(); ++i)
			{
				var tempColour:uint = map.Dimension[i].Colour;
				game.AddColour(tempColour);
				
				game.AddCurrentCog(map.Dimension[i].CurrentCog);
				
				if (map.Dimension[i].Lock == 0)
				{
					game.AddLock(false);
				}
				else
				{
					game.AddLock(true);
				}
				
				var tempPlayer:Clock = new Clock();
				tempPlayer.Init(map.Dimension[i].Player.x, map.Dimension[i].Player.y, map.Dimension[i].Player.radius);
				game.AddPlayer(tempPlayer);
				
				var tempCoord:Coord = new Coord();
				tempCoord.x = map.Dimension[i].Previous.x;
				tempCoord.y = map.Dimension[i].Previous.y;
				game.AddPrevious(tempCoord);
				
				tempCoord = new Coord();
				tempCoord.x = map.Dimension[i].Start.x;
				tempCoord.y = map.Dimension[i].Start.y;
				game.AddStart(tempCoord);
				
				var tempVector:Vector.<int> = new Vector.<int>;
				for (var j:int = 0; j < map.Dimension[i].Rotations.cog.length(); ++j)
				{
					var tempInt:int = map.Dimension[i].Rotations.cog[j];
					tempVector.push(tempInt);
				}
				
				game.AddRotation(tempVector);
			}
			
			game.LoadDimesion(0);
			
			var trans:ColorTransform = new ColorTransform();
			trans.color = game.GetCurrentColour();
			background.transform.colorTransform = trans;
			
			mapLoaded = true;
			
			stage.addChild(dimensionIndicator);
			stage.addChild(timerIndicator);

		}
		
		private function MapLoaderFail(e:IOErrorEvent = null):void
		{
			//need code here
			gameState = 0;
		}
		
		private function ClearScreen():void
		{
			while (stage.numChildren > 1)
			{
				stage.removeChildAt(1);
			}

			mapLoaded = false;
		}
		
		
		private function onEnterFrame(e:Event = null):void
		{
			switch(gameState)
			{
				case 0:
					gameState = menu.Update();
					
					if (gameState == 1)
					{
						LoadMap(menu.GetName());
					}
					break;
					
				case 1:
					if (mapLoaded)
					{
						game.UpdateDimension();
						if (game.dimensionComplete)
						{
							var trans:ColorTransform = new ColorTransform();
							trans.color = game.GetCurrentColour();
							background.transform.colorTransform = trans;
							
							game.dimensionComplete = false;
						}
						if(game.IsLocked())
							dimensionIndicator.text = "Dimension: " + String(game.GetCurrentDimension() + 1) + "*";
						else
							dimensionIndicator.text = "Dimension: " + String(game.GetCurrentDimension() + 1);
						
						timerIndicator.text = String(10 - timer.currentCount);
						
						if (game.levelComplete)
						{
							gameState = 4;
							stage.addChild(levelComplete);
						}
					}
					break;
					
				case 2:
					//paused
					break;
					
				case 3:
					fscommand("quit");
					break;
					
				case 4:
					//game end
					break;
			}
			
		}
		
		private function onKeyDown(e:KeyboardEvent = null):void
		{

			
		}
		
		private function onKeyUp(e:KeyboardEvent = null):void
		{
			if (e.keyCode == Keyboard.SPACE)
			{
				game.Cling();
			}
			
			if (e.keyCode == Keyboard.K)
			{
				game.NextDimension();
				var trans:ColorTransform = new ColorTransform();
				trans.color = game.GetCurrentColour();
				background.transform.colorTransform = trans;
			}
			
			if (e.keyCode == Keyboard.J)
			{
				game.PreviousDimension();
				trans = new ColorTransform();
				trans.color = game.GetCurrentColour();
				background.transform.colorTransform = trans;
			}
			
			if (e.keyCode == Keyboard.P)
			{
				if (gameState == 1)
				{
					gameState = 2;
				}
				else if (gameState == 2)
				{
					gameState = 1;
				}
			}
			
			if (e.keyCode == Keyboard.ESCAPE)
			{
				if (gameState == 1)
				{
					ClearScreen();
					gameState = 0;
					stage.addChild(menu);
					menu.Init();
				}
				else if (gameState == 4)
				{
					ClearScreen();
					gameState = 0;
					stage.addChild(menu);
					menu.Init();
					timer.stop();
				}
			}
		}
		
		private function onTimerEnd(e:TimerEvent):void
		{
			game.ChangeRotation();
			timer.reset();
			timer.start();
		}
		
		private function onClick(e:MouseEvent = null):void
		{
			
		}
		
	}
	
}