package  
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	/**
	 * ...
	 * @author Vimarsh Raina
	 */
	public class Cog extends Sprite
	{
		public var radius:int = 0;
		
		[Embed(source = "../cog.png")]
		private var imgClass:Class;
		private var cogPic:Bitmap = new imgClass;
		
		private var rotate:int = 0;
		
		public function Cog() 
		{
			if (stage)
				init();
			else
				addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		public function Init(_x:int, _y:int, rad:int, rot:int):void
		{
			radius = rad;
			
			cogPic.width = rad * 2;
			cogPic.height = rad * 2;
			
			cogPic.x -= radius;
			cogPic.y -= radius;
			
			x = _x;
			y = _y;
			
			rotate = rot;
			
			addChild(cogPic);
		}
		
		public function Update():void
		{
			if (rotate == -1)
			{
				++rotation;
			}
			else if(rotate == 1)
			{
				--rotation;
			}
			
		}
		
		public function SetRotation(r:int):void
		{
			rotate = r;
		}
		
		public function GetRotation():int
		{
			return rotate;
		}
		
	}

}