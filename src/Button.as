package  
{
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	/**
	 * ...
	 * @author Vimarsh Raina
	 */
	public class Button extends TextField
	{
		
		public function Button() 
		{
			if (stage)
				init();
			else
				addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			this.addEventListener(MouseEvent.MOUSE_OVER, MouseEventFunction);
			this.addEventListener(MouseEvent.MOUSE_OUT, MouseEventFunction);
		}
		
		private function MouseEventFunction(e:MouseEvent):void
		{
			if( e.type == MouseEvent.MOUSE_OVER)
				this.textColor = 0xFF0000;
			else if( e.type == MouseEvent.MOUSE_OUT)
				this.textColor = 0xFFFFFF;
		}
		
		
	}

}