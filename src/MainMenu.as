package  
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	/**
	 * ...
	 * @author Vimarsh Raina
	 */
	public class MainMenu extends Sprite
	{
		[Embed(source="../main.png")]
		private var imgClass:Class;
		private var background:Bitmap = new imgClass;
		
		private var playButton:Button = new Button();
		private var instructButton:Button = new Button();
		private var exitButton:Button = new Button();
		private var backButton:Button = new Button();
		
		private var textFormat:TextFormat = new TextFormat();
		
		private var selection:int = 0;
		private var subMenu:int = -1;
		
		//map loading after pressing play
		private var textInput:TextField = new TextField();
		private var loadButton:Button = new Button();
		private var loadText:TextField = new TextField();
		
		private var instructions:TextField = new TextField();
		
		public function MainMenu() 
		{
			if (stage)
				init();
			else
				addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			textFormat.align = "center";
			textFormat.bold = true;
			textFormat.size = 50;
			
			playButton.textColor = 0xFFFFFF;
			playButton.width = 100;
			playButton.height = 65;
			playButton.defaultTextFormat = textFormat;
			playButton.text = "Play";
			playButton.x = 750;
			playButton.y = 100;
			playButton.addEventListener(MouseEvent.CLICK, onClickPlay);
			
			instructButton.textColor = 0xFFFFFF;
			instructButton.width = 300;
			instructButton.height = 65;
			instructButton.defaultTextFormat = textFormat;
			instructButton.text = "Instructions";
			instructButton.x = 650;
			instructButton.y = 200;
			instructButton.addEventListener(MouseEvent.CLICK, onClickInstruct);
			
			exitButton.textColor = 0xFFFFFF;
			exitButton.width = 100;
			exitButton.height = 65;
			exitButton.defaultTextFormat = textFormat;
			exitButton.text = "Exit";
			exitButton.x = 750;
			exitButton.y = 300;
			exitButton.addEventListener(MouseEvent.CLICK, onClickExit);
			
			loadButton.textColor = 0xFFFFFF;
			loadButton.width = 110;
			loadButton.height = 65;
			loadButton.defaultTextFormat = textFormat;
			loadButton.text = "Load";
			loadButton.x = 750;
			loadButton.y = 300;
			loadButton.addEventListener(MouseEvent.CLICK, onClickLoadMap);
			
			backButton.textColor = 0xFFFFFF;
			backButton.width = 110;
			backButton.height = 65;
			backButton.defaultTextFormat = textFormat;
			backButton.text = "Back";
			backButton.x = 750;
			backButton.y = 400;
			backButton.addEventListener(MouseEvent.CLICK, onClickBack);
			
			textInput.textColor = 0xFFFFFF;
			textInput.border = true;
			textInput.borderColor = 0xFF0000;
			textInput.type = "input";
			textInput.width = 300;
			textInput.height = 65;
			textInput.defaultTextFormat = textFormat;
			textInput.text = "level_1.xml";
			textInput.x = 650;
			textInput.y = 200;
			
			var tF:TextFormat = new TextFormat();
			tF.align = "left";
			tF.size = 30;
			
			loadText.textColor = 0xFFFFFF;
			loadText.width = 500;
			loadText.height = 150;
			loadText.defaultTextFormat = tF;
			loadText.multiline = true;
			loadText.wordWrap = true;
			loadText.text = "Start with 'level_1.xml' and then 'level_2.xml', 'level_3.xml' ..etc";
			loadText.x = 600;
			loadText.y = 100;
			
			instructions.textColor = 0xFFFFFF;
			instructions.width = 1064;
			instructions.height = 600;
			instructions.defaultTextFormat = tF;
			instructions.multiline = true;
			instructions.wordWrap = true;
			instructions.text = "Time is a dimension\n          10 Seconds\n                 10 Dimensions\n\n";
			instructions.appendText("Each level has 10 Dimensions (aka stages), your goal is to reach the start point of\n the next dimension, thus bridging the gap between dimensions.");
			instructions.appendText("There are cogs which\n you must use to move around. Attach / Detach yourself to a cog to move.");
			instructions.appendText("The level is completed when you bridge the gap between the 9th and 10th dimension.");
			instructions.appendText("Only one\n dimension is unlocked at a time. You can only move in an unlocked dimension.");
			instructions.appendText("Also every 10 seconds the direction of the cogs is reversed. The controls are:\n");
			instructions.appendText("WASD - To Move\n\n");
			instructions.appendText("SPACEBAR - Attach / Detach to a cog (Push into the cog)\n\n");
			instructions.appendText("J - Previous Dimension			K - Next Dimension\n\n");
			instructions.appendText("P - Pause Game					ESCAPE - Quit to main menu\n");
			instructions.x = 0;
			instructions.y = 0;
			instructions.background = false;
			
			selection = 0;
		}
		
		private function onClickPlay(e:MouseEvent = null):void
		{
			subMenu = 0;
			stage.removeChild(playButton);
			stage.removeChild(instructButton);
			stage.removeChild(exitButton);
			
			stage.addChild(loadButton);
			stage.addChild(loadText);
			stage.addChild(textInput);
			stage.addChild(backButton);
		}
		
		private function onClickInstruct(e:MouseEvent = null):void
		{
			subMenu = 1;
			stage.removeChild(playButton);
			stage.removeChild(instructButton);
			stage.removeChild(exitButton);
			stage.removeChild(background);
			
			stage.addChild(instructions);
			stage.addChild(backButton);
		}
		
		private function onClickExit(e:MouseEvent = null):void
		{
			selection = 3;
			stage.removeChild(playButton);
			stage.removeChild(instructButton);
			stage.removeChild(exitButton);
			stage.removeChild(background);
		}
		
		private function onClickLoadMap(e:MouseEvent):void
		{
			selection = 1;
		}
		
		private function onClickBack(e:MouseEvent = null):void
		{
			selection = 0;
			
			if (subMenu == 0)
			{
				stage.removeChild(loadButton);
				stage.removeChild(loadText);
				stage.removeChild(textInput);
				stage.removeChild(backButton);
			}
			else if (subMenu == 1)
			{
				stage.removeChild(instructions);
				stage.removeChild(backButton);
			}
			
			subMenu = -1;
			
			Init();
		}
		
		public function Init():void
		{
			stage.addChild(background);
			stage.addChild(playButton);
			stage.addChild(instructButton);
			stage.addChild(exitButton);
			
			selection = 0;
			subMenu = -1;
		}
		
		public function Update():int
		{
			return selection;
		}
		
		public function GetName():String
		{
			return textInput.text;
		}
		
	}

}