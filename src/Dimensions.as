package  
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	/**
	 * ...
	 * @author Vimarsh Raina
	 */
	public class Dimensions extends Sprite
	{
		[Embed(source = "../cog_cling.mp3")]
		private var clingSoundClass:Class;
		private var clingSound:Sound = new clingSoundClass;
		
		[Embed(source = "../next_dimension.mp3")]
		private var nextDimensionSoundClass:Class;
		private var nextDimensionSound:Sound = new nextDimensionSoundClass;
		
		private var clingSoundChannel:SoundChannel = new SoundChannel();
		
		private var cogs:Vector.<Cog>;
		private var colours:Vector.<uint>;
		private var rotations:Vector.<Vector.<int>>;
		private var players:Vector.<Clock>;
		private var currentCogs:Vector.<int>;
		private var previous:Vector.<Coord>;
		private var starts:Vector.<Coord>
		private var lock:Vector.<Boolean>;
		
		private var currentDimension:int = -1;
		
		private const rotationSpeed:int = 2;
		
		public var levelComplete:Boolean = false;
		public var dimensionComplete:Boolean = false;
		
		public function Dimensions() 
		{
			if (stage)
				init();
			else
				addEventListener(Event.ADDED_TO_STAGE, init);
				
			cogs = new Vector.<Cog>;
			colours = new Vector.<uint>;
			rotations = new Vector.<Vector.<int>>;
			players = new Vector.<Clock>;
			currentCogs = new Vector.<int>;
			previous = new Vector.<Coord>;
			starts = new Vector.<Coord>;
			lock = new Vector.<Boolean>;
		}
		
		private function init(event:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		public function IsLocked():Boolean
		{
			return lock[currentDimension];
		}
		//functions to add the dimensions
		public function AddCog(c:Cog):void
		{
			cogs.push(c);
		}
		
		public function AddColour(c:uint):void
		{
			colours.push(c);
		}
		
		public function AddRotation(r:Vector.<int>):void
		{
			rotations.push(r);
		}
		
		public function AddPlayer(p:Clock):void
		{
			players.push(p);
		}
		
		public function AddCurrentCog(cc:int):void
		{
			currentCogs.push(cc);
		}
		
		public function AddPrevious(pv:Coord):void
		{
			previous.push(pv);
		}
		
		public function AddStart(s:Coord):void
		{
			starts.push(s);
		}
		
		public function AddLock(l:Boolean):void
		{
			lock.push(l);
		}
		
		//accessor functions
		public function GetCurrentColour():uint
		{
			return colours[currentDimension];
		}
		
		public function GetCurrentDimension():int
		{
			return currentDimension;
		}
		
		public function ChangeRotation():void
		{
			for (var i:int = 0; i < cogs.length; ++i)
			{
				cogs[i].SetRotation(cogs[i].GetRotation() * -1);
			}
		}
		
		public function Cling():void
		{
			var collision:Boolean = false;
			
			for (var i:int = 0; i < cogs.length; ++i)
			{
				if (cogs[i].GetRotation() == 0)
						continue;
				
				if (currentCogs[currentDimension] != -1)
				{
					if (currentCogs[currentDimension] == i)
					{
						continue;
					}
				}
				
				var dx:int = (players[currentDimension].x + 0) - cogs[i].x;
				var dy:int = (players[currentDimension].y + 0) - cogs[i].y;
				
				dx *= dx;
				dy *= dy;
				
				var dr:Number = Math.sqrt(dx + dy);
				
				if (dr <= (players[currentDimension].radius + cogs[i].radius))
				{
					starts[currentDimension].x =  (((players[currentDimension].x + 0) * cogs[i].radius) + (cogs[i].x * players[currentDimension].radius)) / (players[currentDimension].radius + cogs[i].radius);
					starts[currentDimension].y =  (((players[currentDimension].y + 0) * cogs[i].radius) + (cogs[i].y * players[currentDimension].radius)) / (players[currentDimension].radius + cogs[i].radius);
					
					starts[currentDimension].x -= cogs[i].x;
					starts[currentDimension].y -= cogs[i].y;
					
					currentCogs[currentDimension] = i;
					collision = true;
					
					clingSoundChannel = clingSound.play();
					break;
				}
			}
			
			if (!collision && currentCogs[currentDimension] != -1)
			{
				currentCogs[currentDimension] = -1;
				clingSoundChannel = clingSound.play();
			}
			else if (!collision)
			{
				currentCogs[currentDimension] = -1;
			}
		}
		
		public function NextDimension():void
		{
			if ((currentDimension + 1) >= players.length)
			{
				LoadDimesion(0);			
			}
			else
			{
				LoadDimesion(currentDimension + 1);
			}
		}
		
		public function PreviousDimension():void
		{
			if ((currentDimension - 1) < 0)
			{
				LoadDimesion(players.length - 1);			
			}
			else
			{
				LoadDimesion(currentDimension - 1);
			}
		}
		
		public function LoadDimesion(d:int):void
		{				
			if (currentDimension != -1)
			{
				stage.removeChild(players[currentDimension]);
				
				stage.addChild(players[d]);
				for (var i:int = 0; i < cogs.length; ++i)
				{
					cogs[i].SetRotation(rotations[d][i]);
				}
				
				currentDimension = d;
			}
			else
			{
				stage.addChild(players[d]);
				currentDimension = d;
				for (i = 0; i < cogs.length; ++i)
				{
					cogs[i].SetRotation(rotations[d][i]);
				}
			}
		}
		
		public function UpdateDimension():void
		{
			for (var i:int = 0; i < cogs.length; ++i)
			{
				cogs[i].Update();
			}
			if (lock[players.length - 1] == false)
			{
				levelComplete = true;
			}
			var nextDimension:int = currentDimension + 1;
			if (nextDimension >= players.length)
			{
				
			}
			else if (players[currentDimension].hitTestObject(players[nextDimension]) && lock[currentDimension] == false)
			{
				lock[currentDimension] = true;
				lock[nextDimension] = false;
				
				NextDimension();
				
				dimensionComplete = true;
				
				clingSoundChannel.stop();
				clingSoundChannel = nextDimensionSound.play();
			}

			if (currentCogs[currentDimension] == -1)
			{
				for (i = 0; i < cogs.length; ++i)
				{
					var dx:int = (players[currentDimension].x + 0) - cogs[i].x;
					var dy:int = (players[currentDimension].y + 0) - cogs[i].y;
					
					dx *= dx;
					dy *= dy;
					
					var dr:Number = Math.sqrt(dx + dy);
					
					if (dr <= (players[currentDimension].radius + cogs[i].radius))
					{
						if (previous[currentDimension].x != 0 && previous[currentDimension].y != 0)
						{
							players[currentDimension].x = previous[currentDimension].x;
							players[currentDimension].y = previous[currentDimension].y;
						}
						break;
					}
				}
				
				previous[currentDimension].x = players[currentDimension].x;
				previous[currentDimension].y = players[currentDimension].y;
				
				if(lock[currentDimension] == false)
					players[currentDimension].Update();
			}
			else if(lock[currentDimension] == false)
			{
				if (starts[currentDimension].x >= 0 && starts[currentDimension].y >= 0)
				{
					//++startX;
					//--startY;
					
					starts[currentDimension].x += rotationSpeed * cogs[currentCogs[currentDimension]].GetRotation();
					starts[currentDimension].y += -rotationSpeed * cogs[currentCogs[currentDimension]].GetRotation();
				}
				else if (starts[currentDimension].x >= 0 && starts[currentDimension].y <= 0)
				{
					//--startX;
					//--startY;
					
					starts[currentDimension].x += -rotationSpeed * cogs[currentCogs[currentDimension]].GetRotation();
					starts[currentDimension].y += -rotationSpeed * cogs[currentCogs[currentDimension]].GetRotation();
				}
				else if (starts[currentDimension].x <= 0 && starts[currentDimension].y <= 0)
				{
					//--startX;
					//++startY;
					
					starts[currentDimension].x += -rotationSpeed * cogs[currentCogs[currentDimension]].GetRotation();
					starts[currentDimension].y += rotationSpeed * cogs[currentCogs[currentDimension]].GetRotation();
				}
				else
				{
					//++startX;
					//++startY;
					
					starts[currentDimension].x += rotationSpeed * cogs[currentCogs[currentDimension]].GetRotation();
					starts[currentDimension].y += rotationSpeed * cogs[currentCogs[currentDimension]].GetRotation();
				}
				
				var dX:Number = starts[currentDimension].x;
				var dY:Number = starts[currentDimension].y;
				var unit:Number = Math.sqrt((dX * dX) + (dY * dY));
				
				dX = dX / unit;
				dY = dY / unit;
				
				var sum:Number = players[currentDimension].radius + cogs[currentCogs[currentDimension]].radius;
				
				previous[currentDimension].x = players[currentDimension].x;
				previous[currentDimension].y = players[currentDimension].y;
				
				players[currentDimension].x = cogs[currentCogs[currentDimension]].x + (dX * sum);
				players[currentDimension].y = cogs[currentCogs[currentDimension]].y + (dY * sum);
			}
		}
	}

}