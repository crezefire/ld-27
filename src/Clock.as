package  
{
	import flash.display.Bitmap;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.ui.Keyboard;
	/**
	 * ...
	 * @author Vimarsh Raina
	 */
	public class Clock extends Sprite
	{
		[Embed(source = "../clock.png")]
		private var imgClass:Class;
		private var clockPic:Bitmap = new imgClass;
		
		public var radius:int;
		
		private var movingLeft:Boolean = false;
		private var movingRight:Boolean = false;
		private var movingUp:Boolean = false;
		private var movingDown:Boolean = false;
		
		private const speed:int = 10;
		
		public function Clock() 
		{
			if (stage)
				init();
			else
				addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(event:Event = null):void
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			
			stage.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
			stage.addEventListener(KeyboardEvent.KEY_UP, onKeyUp);
		}
		
		public function Init(_x:int, _y:int, rad:int):void
		{
			x = _x;
			y = _y;
			
			radius = rad;
			
			clockPic.width = rad * 2;
			clockPic.height = rad * 2;
			
			clockPic.x -= rad;
			clockPic.y -= rad;
			
			addChild(clockPic);
		}
		
		public function Update():void
		{
			if (movingDown)
			{
				y += speed;
				if ((y + radius) > stage.stageHeight)
				{
					y -= speed;
				}
			}
			
			if (movingRight)
			{
				x += speed;
				if ((x + radius) > stage.stageWidth)
				{
					x -= speed;
				}
			}
			
			if (movingLeft)
			{
				x -= speed;
				if ( (x - radius) < 0)
				{
					x += speed;
				}
			}
			
			if (movingUp)
			{
				y -= speed;
				if ( (y - radius) < 0)
				{
					y += speed;
				}
			}
		}
		
		private function onKeyUp(e:KeyboardEvent = null):void
		{
			if (e.keyCode == Keyboard.W)
			{
				movingUp = false;
			}
			
			if (e.keyCode == Keyboard.A)
			{
				movingLeft = false;
			}
			
			if (e.keyCode == Keyboard.S)
			{
				movingDown = false;
			}
			
			if (e.keyCode == Keyboard.D)
			{
				movingRight = false;
			}
		}
		
		private function onKeyDown(e:KeyboardEvent = null):void
		{
			if (e.keyCode == Keyboard.W)
			{
				movingUp = true;
			}
			
			if (e.keyCode == Keyboard.A)
			{
				movingLeft = true;
			}
			
			if (e.keyCode == Keyboard.S)
			{
				movingDown = true;
			}
			
			if (e.keyCode == Keyboard.D)
			{
				movingRight = true;
			}
		}
		
	}

}